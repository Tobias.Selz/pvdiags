
#mean earth radius
r_e = 6371e3

#reference pressure for potential temperature (Pa)
p00 = 1e5

#adiabat exponent
kappa = 287.04/1004.64

