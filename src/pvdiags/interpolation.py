import xarray as xr
import numpy as np
from numba import jit


def vertical_interpolation(dsin, coord_new, dims, levs_new, fill_ambiguity='nan', fill_outbounds=False):
    """Vertically interpolate the dataset, e.g. from model- to pressure or isentropic levels.
    
       Args:
           dsin (xarray dataset): data to interpolate to the new vertical coordinate
           coord_new (xarray dataarray or str): values of the new coordinate variable on the old vertical levels. If a string is given, the variable will be picked from dsin
           dims (tuple of str): labels of the old and the new dimension (coordinate)
           levs_new (iterable): leves of the new coord
           fill_ambiguity ('nan', 'lowest', highest'): If the new coordinate is non-monotonic at a point and several levels exist, fill in nan or replace with the highest/lowest index level (not value!) according to the old coordinate
           fill_outbounds (bool): If the new coordinate value is outside the range, put nan (False) or replace with the value at the closest boundary (True)
    """
    
    if isinstance(coord_new, str):
        var = coord_new
        coord_new = dsin[var]
        #remove the variable from dsin
        dsin = dsin.drop_vars(var)
        
    #loop through all variables in the dataset, and split the dataset in a part that needs interpolation and one that doesn't
    dskeep = xr.Dataset()
    dsint = xr.Dataset()
    #loop through data arrays
    for var in dsin.data_vars:
        if dims[0] not in dsin[var].dims:
            dskeep[var] = dsin[var]
        else:
            dsint[var] = dsin[var]
    #get a list of dimensions to preserve the order later
    somevar = list(dsin.data_vars)[0]
    dimlist = list(dsin[somevar].dims)
    #replace the old dim name by the new one in dimlist
    dimlist = [d if d!=dims[0] else dims[1] for d in dimlist]
    #perform the interpolation
    dsnew, mask = xr.apply_ufunc(_interpol_lin_1D, dsint, coord_new, kwargs={"levs_new": np.array(levs_new), 'fill_ambiguity': fill_ambiguity, 'fill_outbounds': fill_outbounds},\
                                 input_core_dims=[[dims[0]], [dims[0]]], output_core_dims=[[dims[1]], [dims[1]]], vectorize=True, dask="parallelized",output_sizes={dims[1]: len(levs_new)})
    #the mask is identical for all variables, just keep the first one
    var = list(mask.data_vars)[0]
    mask = mask[var].astype('bool')
    dsnew = dsnew.assign_coords({dims[1]: levs_new})
    dsnew = dsnew.transpose(*dimlist)
    #add the mask to the dsnew-coords, unless it is never true
    if np.any(mask):
        mask = mask.assign_coords({dims[1]: levs_new})
        mask = mask.transpose(*dimlist)
        dsnew['interp_mask'] = mask
    else:
        print('no mask')
    return xr.merge([dsnew, dskeep])
        

@jit(nopython=True)    
def _interpol_lin_1D(field, coord_new, levs_new, fill_ambiguity, fill_outbounds):
    """Interpolates field onto new levels given by the array coord_newlevs. The first dimension will be interpolated"""
    if coord_new.ndim!=1 or field.ndim!=1:
        raise Exception('Fields have to be 1 dimensional')
    nlev_old = len(coord_new) 
    field_new = np.zeros((len(levs_new)), dtype=np.float64)
    mask = np.zeros((len(levs_new)), dtype=np.int32)
    for ilev_new, lev_new in enumerate(levs_new):
        #collect all interpoltaion values in a list
        intvals = []
        for ilev_old in range(nlev_old-1):
            if (coord_new[ilev_old]>=lev_new and coord_new[ilev_old+1]<lev_new) or (coord_new[ilev_old]<=lev_new and coord_new[ilev_old+1]>lev_new):
                a = (lev_new-coord_new[ilev_old+1]) / (coord_new[ilev_old]-coord_new[ilev_old+1])
                intvals.append((1-a)*field[ilev_old+1] + a*field[ilev_old])
        if len(intvals)==1:
            #only one new level found
            field_new[ilev_new] = intvals[0]
        elif len(intvals)>1:
            #several new levels found, precide according to ambiguity parameter
            if fill_ambiguity=='nan':
                field_new[ilev_new] = np.nan
            elif fill_ambiguity=='lowest':
                field_new[ilev_new] = intvals[0]
            elif fill_ambiguity=='highest':
                field_new[ilev_new] = intvals[-1]
            else:
                raise Exception('Unknown ambiguity parameter')
            #independet of the ambiguity parameter, mask indicate the failed interpolation in the mask
            mask[ilev_new] = 1
        elif len(intvals)==0:
            #no level found, i.e. the level is below the ground or above the top
            #it can happen that the level is exactly equal to the boundary, in this case just set it
            if lev_new==coord_new[-1]:
                field_new[ilev_new] = field[-1]
            elif lev_new==coord_new[0]:
                field_new[ilev_new] = field[0]
            else:
                #level is outside the given range. Fill with the boundary that is closest to the requested level (this requires no assumption on order)
                if fill_outbounds:
                    if abs(lev_new-coord_new[0])<=abs(lev_new-coord_new[-1]):
                        field_new[ilev_new] = field[0]
                    else:
                        field_new[ilev_new] = field[-1]
                else:
                    field_new[ilev_new] = np.nan
                mask[ilev_new] = 1
                
    return field_new, mask


