import xarray as xr
import numpy as np
from .constants import r_e
import copy


#necessary helper functions
def get_incr(coord, check_ascending=True):
    """Checks if the coordinate is equidistant and returns the increment. If check_ascending is true, an exception will be raised if the increment is <0."""
    vals = coord.values
    nvals = len(vals)
    incr = vals[1] - vals[0]
    checkvals = vals[0] + incr * np.arange(nvals)
    #if not np.all(vals==checkvals):
    #    raise Exception('Coordinate is not equidistant')
    if check_ascending and incr<0:
        raise Exception('Coordinate is not in ascending order')
    return incr

def is_periodic(coord):
    """Checks if the coord (in degree) is periodic."""
    incr = get_incr(coord)
    #get first point and last point +1
    p0 = coord.values[0]
    p1 = coord.values[-1] + incr
    return (p1-p0)*np.sign(incr)==360.


def get_gridcell_area(dain):
    dlon = get_incr(dain.lon)
    dlat = get_incr(dain.lat)
    dA = r_e**2 * np.deg2rad(dlon) * np.cos(np.deg2rad(dain.lat)) * np.deg2rad(dlat)
    dA, _ = xr.broadcast(dA, dain.lon)
    return dA


def area_integration(dsin, intarea, normalize=False):
    """
    Calculates area integrals of fields defined on lat/lon coordinates.
    
    Args:
        dsin (dataset or dataarray): Input fields that should be integrated
        intarea (dict): Area of integration. A dict with keys lon1, lon2, lat1, lat2
        normalize (bool, optional): If True, normalize the area weights
        
    Returns:
        dataset or dataarray: Result of the area integration
    """
    
    # Make a copy of the intarea dictionary to avoid modifying the original 
    intarea = copy.deepcopy(intarea)

    # check if dsin is periodic in lon
    if is_periodic(dsin.lon):
        #check if longitude goes from -180 to 179 or 0 to 360
        if dsin.lon[0].values == -180:
            # shift data to enable easy periodic slicing
            dsin = convert_lon_180_to_360(dsin)
            # Shift the longitude coordinates by 180 degrees
            intarea["lon1"] += 180
            intarea["lon2"] += 180
        elif dsin.lon[0].values != 0:
            raise ValueError("Longitude coordinate of a periodic DataSet/DataArray must fall within either the range of -180 to 179 degrees or the range of 0 to 360 degrees.")
    
    # Directly slice the longitude dimension while handling periodicity
    if intarea["lon1"] < intarea["lon2"]:
        dsin_subset = dsin.sel(lon=slice(intarea["lon1"], intarea["lon2"]), lat=slice(intarea['lat1'], intarea['lat2']))

    else:
        dsin_subset = dsin.sel(lon=slice(intarea["lon1"], None)).combine_first(dsin.sel(lon=slice(None,  intarea["lon2"])))
        dsin_subset = dsin_subset.sel(lat=slice(intarea['lat1'], intarea['lat2']))
    
    if isinstance(dsin, xr.Dataset):
        aint = {}
        # iterate over all dataarrays and integrate
        for var_name, data_array in dsin_subset.items():
            aint[var_name] = _calculate_integral(data_array, normalize)
        # combine data_arrays into dataset
        aint = xr.Dataset(aint)
    else:
        aint = _calculate_integral(dsin_subset, normalize)
    # reshift longitude coordinates to its orignial shape
    return aint

def _calculate_integral(dsin, normalize):
    dA = get_gridcell_area(dsin)
    dA = xr.where(np.isnan(dsin), 0, dA)
    dsin = xr.where(np.isnan(dsin), 0, dsin)
    
    if normalize:
        dA /= dA.sum(['lon', 'lat'])
    
    return (dsin * dA).sum(['lon', 'lat'])


def convert_lon_180_to_360(dsin):
    """
    Convert longitudes from -180 to 180 range to 0 to 360 range.
    
    Parameters:
        dsin (xarray.Dataset or xarray.DataArray): Input dataset with longitude variable.
    
    Returns:
        xarray.Dataset or xarray.DataArray: Dataset with longitude variable converted to 0 to 360 range.
    """
    # Ensure that the input is a dataset or data array
    if not isinstance(dsin, (xr.Dataset, xr.DataArray)):
        raise ValueError("Input must be an xarray Dataset or DataArray.")
    
    # Extract the 'lon' dimension
    lon_dim = 'lon' if 'lon' in dsin.dims else None
    
    # Check if 'lon' dimension is found
    if lon_dim is None:
        raise ValueError("Input dataset must have a 'lon' dimension.")
    
    # Roll the longitude dimension so that 0 degrees is at the beginning
<<<<<<< HEAD
    ds_shifted = dsin.roll({lon_dim: (dsin.sizes[lon_dim] // 2)})
=======
    ds_shifted = dsin.roll(lon=dsin.sizes['lon'] // 2)
>>>>>>> remotes/origin/develop

    # Assign new coordinates to the rolled dataset
    ds_shifted = ds_shifted.assign_coords({lon_dim: ((ds_shifted[lon_dim] + 180) % 360)})

    return ds_shifted


def ddx(dain, calc_bnds=False):
    #get the grid spacing
    dlon = get_incr(dain.lon)
    #apply central differences
    res = dain.differentiate('lon', edge_order=1)
    #check if lon is periodic and if so, overwrite the boundaries
    if is_periodic(dain.lon):
        res[dict(lon=0)] = (dain.isel(lon=1) - dain.isel(lon=-1)) / (2*dlon)
        res[dict(lon=-1)] = (dain.isel(lon=-2) - dain.isel(lon=0)) / (2*dlon)
    elif not calc_bnds:
        res[{'lon': 0}] = np.nan
        res[{'lon': -1}] = np.nan
    dx = r_e * np.deg2rad(dlon) * np.cos(np.deg2rad(dain.lat))   
    return res * dlon/dx


def ddy(dain, calc_bnds=False):
    #get the grid spacing
    dlat = get_incr(dain.lat)
    #apply central differences
    res = dain.differentiate('lat', edge_order=1)
    if not calc_bnds:
        res[{'lat': 0}] = np.nan
        res[{'lat': -1}] = np.nan
    dy = r_e * np.deg2rad(dlat)
    return res * dlat/dy

def hor_divergence(dau, dav, calc_bnds=False):
    return ddx(dau, calc_bnds) + ddy(dav, calc_bnds) - dav / r_e * np.tan(np.deg2rad(dav.lat))



