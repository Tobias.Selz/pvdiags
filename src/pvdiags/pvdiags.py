import xarray as xr
import numpy as np
from pvdiags.constants import p00, kappa
from pvdiags.grid_functions import ddx, ddy, hor_divergence, area_integration

#TODO: implement edge_order, calc_bnds


def calc_theta(dsin):
    da_theta = dsin.temp * (dsin.pres/p00)**(-kappa)
    da_theta.name = 'theta'
    return da_theta
    

def calc_pvdiags(dsin, intarea=None, integrate=False, calc_bnds=False):
    """Calculates the pv-diagnostics on isentropic levels for an ensemble.

       Args:
           dsin (xarray dataset): Necessary input fields. Must contain pv, urot, vrot, udiv, vdiv, Qx, ddth_Qx, ddth_pv.
               x can stand for any specific process. Can also contain several urot, vrot to be used together with piecewise pv inversion.
               Data must be defined on isentropic levels (dim=thlev) and on regular lat-lon grids.
           
           intarea (dict): specify lat1, lat2, lon1, lon2 in degrees

           calc_bnds (bool): If true, single-sided differences will be used to calculate the gradients at the lat/lon boundaries.
           
       Returns:
           xarray dataset that contains the process-specific enstrophy tendencies, named: Trot, Tdiv, Tx
    """

    
    #Check if the dataset has the necessary coordinates: thlev, lat, lon, ens
    
    #Check if the lat and lon coordinates are regular and in ascending order. Compute dlat, dlon
    
    #Check if the given lon-domain is periodic
    
    
    allvars = list(dsin.variables)
    
    #Check if pv is present
    if not 'pv' in allvars:
        raise Exception('pv variable missing in input data')
    
    #Check if rotational component can be calculated and create a list of the rotwind labels
    rotvars = []
    for var in allvars:
        if var.startswith('urot'):
            #Check, if corresponding vrot is present
            rotvar = var[1:]
            if f"v{rotvar}" in allvars:
                rotvars.append(rotvar)
                
    #Check if divergent component can be calculated
    if 'udiv' in allvars and 'vdiv' in allvars:
        divvars = ['div']
    else:
        divvars = []
    
    #Check if diabatic tendencies can be calculated and create a list of the process labels
    qvars = []
    for var in allvars:
        if var.startswith('Q'):
            qvar = var[1:]
            #check, if corresponding theta-derivative and pv-derivative is present
            if f"ddth_Q{qvar}" in allvars and f"ddth_pv" in allvars:
                qvars.append(qvar)

    #PV-tendencies calculation
    #create a dict to store the tendencies
    Tdict = xr.Dataset()
    
    #Calculate the horizontal gradient of ensmean pv if required
    if len(rotvars+divvars)>=1:
        ddx_mpv = ddx(dsin.pv.mean('ens'), calc_bnds=calc_bnds)
        ddy_mpv = ddy(dsin.pv.mean('ens'), calc_bnds=calc_bnds)
        
    #Calculate the rotational tendencies
    for rotvar in rotvars:
        Tdict[f"T{rotvar}"] = -2*xr.cov(dsin.pv, dsin[f"u{rotvar}"], dim='ens')*ddx_mpv -2*xr.cov(dsin.pv, dsin[f"v{rotvar}"], dim='ens')*ddy_mpv
        
    #Calculate divergent tendencies
    for divvar in divvars:
        Tdict[f"T{divvar}"] = -2*xr.cov(dsin.pv, dsin[f"u{divvar}"], dim='ens')*ddx_mpv -2*xr.cov(dsin.pv, dsin[f"v{divvar}"], dim='ens')*ddy_mpv \
                              + dsin.pv.var(dim='ens',ddof=1)*hor_divergence(dsin[f"u{divvar}"].mean('ens'), dsin[f"v{divvar}"].mean('ens'), calc_bnds=calc_bnds) 
        
    #Calculate the diabatic terms
    for qvar in qvars:
        Tdict[f"T{qvar}"] = -2*xr.cov(dsin.pv, dsin[f"Q{qvar}"], dim='ens')*dsin.ddth_pv.mean('ens') - 2*xr.cov(dsin.pv, dsin.ddth_pv, dim='ens')*dsin[f"Q{qvar}"].mean('ens') \
                            +2*xr.cov(dsin.pv, dsin.pv, dim='ens')*dsin[f"ddth_Q{qvar}"].mean('ens') + 2*xr.cov(dsin.pv, dsin[f"ddth_Q{qvar}"], dim='ens')*dsin.pv.mean('ens')
                            
    #add the potential enstrophy variance
    Tdict["P"] = dsin.pv.var('ens')
    
    #integrate if requested
    if intarea is not None:
        for var in Tdict.keys():
            Tdict[var]= area_integration(Tdict[var], intarea, normalize=False)
    
    return Tdict


