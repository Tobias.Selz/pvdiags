.. pvdiags documentation master file, created by
   sphinx-quickstart on Tue Aug  1 13:55:50 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pvdiags's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro.rst
   installation.rst
   usage.rst
   api_reference.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
