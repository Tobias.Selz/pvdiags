import xarray as xr
import pvdiags
import pandas as pd
from datetime import datetime, timedelta
import windspharm
from matplotlib import pyplot as plt
import numpy as np


infiles_ua = "/project/meteo/w2w/A1/icon_data/w2w_exp3/output_0.0010_20161001_R2B6_#mem/NWP_UA_lonlat_ML_*.nc"
infiles_tend = "/project/meteo/w2w/A1/icon_data/w2w_exp3/output_0.0010_20161001_R2B6_#mem/NWP_TEND_lonlat_ML_*.nc"

memlist = [1, 10]

times = pd.date_range('2016-10-01', '2016-10-02', freq='1H')

thlevs = [325.]

proclist = ['pconv', 'gscp', 'radlw']

cpocv = 1.403

#concatenate the UA dataset    filemask = gl.get_filemask(exp, case, mem, 'TEND').replace('#ifile', '*')

dsmem = []
for mem in memlist:
    dsmem.append(xr.open_mfdataset(infiles_ua.replace('#mem', f'{mem:02}')).sel(time=times)[['pv', 'pres', 'temp', 'u', 'v']])
dsua = xr.concat(dsmem, 'ens')

#concatenate the tend dataset
dsmem = []
for imem, mem in enumerate(memlist):
    ds = xr.open_mfdataset(infiles_tend.replace('#mem', f'{mem:02}')).sel(time=times)
    ds_tend = xr.Dataset()
    for proc in proclist:
        var = f"ddt_temp_{proc}_acc"
        #get instantaneous tendency
        da_tend = ds[var].differentiate('time', edge_order=1, datetime_unit='s')
        #convert to theta tendency and divide by cv/cp
        da_tend *= (dsua.pres.isel(ens=imem)/pvdiags.constants.p00)**(-pvdiags.constants.kappa) / cpocv
        ds_tend[f'Q{proc}'] = da_tend
    dsmem.append(ds_tend)
ds_tend = xr.concat(dsmem, 'ens').compute()  

#calculate theta and include it
theta = pvdiags.calc_theta(dsua)
dsua = dsua.merge(theta).compute()

ds = xr.merge([dsua, ds_tend])

#compute the theta-derivative of Qx and pv
for var in ['pv'] + [f"Q{proc}" for proc in proclist]:
    ds[f"ddth_{var}"] = ds[var].differentiate('height', edge_order=1) / ds.theta.differentiate('height', edge_order=1)

#interpolate to theta levels
ds = pvdiags.interpolation.vertical_interpolation(ds, 'theta', ('height', 'thlev'), thlevs, fill_ambiguity='lowest', fill_outbounds=True)

#compute the Helmholtz-decomposition using the windspharm xarray interface
wsph = windspharm.xarray.VectorWind(ds.u, ds.v, rsphere=pvdiags.constants.r_e, legfunc='stored')
udiv, vdiv, urot, vrot = wsph.helmholtz()
ds = ds.merge(xr.Dataset({'udiv': udiv, 'vdiv': vdiv, 'urot': urot, 'vrot': vrot})).astype('float32')


ds_pvd = pvdiags.calc_pvdiags(ds.isel(ens=[0, 1]), intarea={'lon1': -180, 'lon2': 179, 'lat1': 20, 'lat2': 80}, integrate=True)


